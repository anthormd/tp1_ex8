from setuptools import setup

setup(
    name='SimpleCalculator',
    version='0.0.1',
    author="Anthony Raymond",
    packages=['calculator','calculator/'],
    description="Ecriture d'une classe SimpleCalcultor pour \
        apprendre les principes de packages sous python",
    license='GNU GPLv3',
    python_requires ='>=3.4',
)
